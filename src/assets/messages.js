module.exports = Object.freeze({
    "forms": {
            "notEmail": "Doit contenir une adresse mail (ex : xxxx@xxxx.xx)",
            "notPassword": "Le mot de passe doit contenir au moins une majuscule, une minuscule, un chiffre et 8 caractères.",
            "requiredField": "Ce champs est obligatoire."
        }
})