import {AxiosInstance as axios} from "axios";
import qs from "querystring";

export function tokenGetter() {
    let url = "http://192.168.100.171:3703/auth/realms/AGT_SSO/protocol/openid-connect/token";
    const body = {
        client_id: "Simeos",
        grant_type: "password",
        scope: "openid",
        username: "alicherif.samir@gmail.com",
        password: 123,
    }

    axios.post(url, qs.stringify(body))
        .then(r => {
            return r.access_token;
        })
        .catch(err => {
            console.log("Erreur dans la demande du token");
            console.log(err);
        })
}